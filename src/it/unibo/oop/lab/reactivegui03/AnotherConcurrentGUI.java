package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * 
 * 
 *
 */
public class AnotherConcurrentGUI extends JFrame {
    /**
     * 
     */
    private static final long serialVersionUID = 3588025866426941462L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("Up");
    private final JButton down = new JButton("Down");
/**
 * 
 */
    public AnotherConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);

        final Agent agent = new Agent();
        new Thread(agent).start();

        stop.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                // Agent should be final
                agent.stopCounting();
                up.setEnabled(false);
                down.setEnabled(false);
            }
        });

        up.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
               agent.setUp();

            }
        });

        down.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
               agent.setDown();

            }
        });
      //perchè va dopo i listener?*** (non andavano tasti se no)
      //runnable sul thread specificando cosa fare (stopCounting)
        final AgentEnd end = new AgentEnd(() -> agent.stopCounting());
        new Thread(end).start(); 

    }

        private class Agent implements Runnable {
            /*
             * stop is volatile to ensure ordered access
             */
            private volatile boolean stop;
            private volatile boolean down;
            private int counter;

            public void run() {
                while (!this.stop) {
                    try {
                        /*
                         * All the operations on the GUI must be performed by the
                         * Event-Dispatch Thread (EDT)!
                         */
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                            }
                        });
                        if (down) {
                            this.counter--;
                        } else {
                            this.counter++;
                        }
                        Thread.sleep(100);
                    } catch (InvocationTargetException | InterruptedException ex) {
                        /*
                         * This is just a stack trace print, in a real program there
                         * should be some logging and decent error reporting
                         */
                        ex.printStackTrace();
                    }
                }
            }

            /**
             * External command to stop counting.
             */
            public void stopCounting() {
                this.stop = true;
            }

            public void setUp() {
                this.down = false;
            }

            public void setDown() {
                this.down = true;
            }

        }

        private class AgentEnd implements Runnable {

            public AgentEnd(final Runnable action) {
                try {
                    Thread.sleep(10000); //10 sec
                    action.run();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } 
            }

            @Override
            public void run() {
                try {

                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            up.setEnabled(false);
                            down.setEnabled(false);
                        }
                    });

                } catch (InterruptedException | InvocationTargetException  e) {
                    e.printStackTrace();
                }
            }
        }
}
